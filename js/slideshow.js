/*
*Javascript to cycle through images
*/

//Variables
var index = 1;
var pictures = ["apple_invite.jpg", "apple_stand.jpg", "apple_sign.jpg", "apple_band.jpg"];
var main_image = document.getElementById('hero_img');


//*Basic function to cycle through applefest images
function cycle() {
	"use strict";
	if (index < pictures.length) {
        main_image.src = 'images/' + pictures[index];
        index += 1;
    } else {
        index = 0;
        main_image.src = 'images/' + pictures[index];
        index += 1;
    }
}

//*function using cycle() at certain intervals as determined by numerical value
function interval() {
    "use strict";
    setInterval(cycle, 5000);
}

//Calling the function
interval();
