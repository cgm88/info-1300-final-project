/**A javascript to change the date in the footer in the html file. **/

//Function to add the date to footer in the html file
window.onload = function getToday() {
    "use strict";
    //Variables
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    //Create a new Date
    theDate = new Date(),
    //Get the date(number), month(string)), and year(number) 
    date = theDate.getDate(),
    month = months[theDate.getMonth()],
    year = theDate.getFullYear(),
    //Create a text that combines date, month, year
    today = document.createTextNode(date + " " + month + " " + year + " " + "|" +" " + "Apple Festival"),
    span = document.createElement('span'),
    position = document.getElementById('datebox');
    //Append the date generated to the footer
    span.appendChild(today);
    position.appendChild(span);
};
